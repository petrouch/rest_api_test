<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhoneBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('phone_books', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('timezone_id')->nullable();
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onUpdate('cascade');

            $table->foreign('timezone_id')
                ->references('id')->on('timezones')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_books');
    }
}
