# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

 Разработайте RESTful API для хранения, вывода, удаления и обновления записей в
телефонной книге;
2. Запись в телефонной книге должна иметь следующие поля:
* a. Имя;
* b. Фамилия;
* c. Номер телефона (В формате рекомендации E.164);
* d. Код страны. Данные для валидации;
* e. Часовой пояс. Данные для валидации;
* f. Дата и время создания;
* g. Дата и время обновления.
3. На каждую вставку и обновление должен отправляться запрос на получение кодов
стран и часовых поясов для дальнейшей валидации входных данных. В случае
некорректного ввода приложение должно отдать соответствующую ошибку;
4. Исключения должны обрабатываться должным образом, особенно это касается
валидации и исключений в процессе выполнения HTTP-запросов;
5. Слои приложения должны быть разделены должным образом;
6. При необходимости должны использоваться соответствующие шаблоны
проектирования;
7. Выводить записи нужно списком, а также поиском по ID и по частям ФИО.
