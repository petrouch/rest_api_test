<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix'=>'api/phone-book'], function() use($router){
    // Get all records
    $router->get('records', 'PhoneBookController@getRecords');
    // Get record by Id
    $router->get('records/{id}', 'PhoneBookController@getRecordById');
    // Get record by First Name and Last Name
    $router->get('recordsByName/{name}', 'PhoneBookController@getRecordsByName');


    // Delete record
    $router->delete('records/{id}', 'PhoneBookController@destroy');
    // Update record
    $router->put('records/{id}', 'PhoneBookController@update');
    // Create record
    $router->post('records', 'PhoneBookController@create');
});

$router->group(['prefix'=>'phone-book'], function() use($router){
    // Get all records
    $router->get('refresh/timezones', 'TimezoneController@refreshData');
    // Get record by Id
    $router->get('refresh/countries', 'CountryController@refreshData');
});
