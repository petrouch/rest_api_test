<?php

namespace App\Http\Interfaces;

use GuzzleHttp\Client;

/**
 * @property string $password
 */
interface RefreshApiInterface
{
    /**
     * Refresh data in database
     *
     * @return string
     */
    public function refreshData();
}