<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Interfaces\RefreshApiInterface;
use GuzzleHttp\Client;

/**
 * Class CountryController
 * @package App\Http\Controllers
 */
class CountryController extends Controller implements RefreshApiInterface
{
    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory|string
     */
    public function refreshData()
    {
        $new_countries = $this->getDataFromUrl('https://experum.ru/te/country-codes');

        $old_countries = Country::all()->keyBy('code');

        foreach ($new_countries as $code => $name) {
            if (isset($old_countries[$code]))
            {
                if ($old_countries[$code]->name !== $name) {
                    $old_countries[$code]->name = $name;
                }
            } else {
                $old_countries[$code] = new Country();

                $old_countries[$code]->code = $code;
                $old_countries[$code]->name = $name;
            }

            $old_countries[$code]->save();
        }

        return response('Refreshed Successfully', 200);
    }

    /**
     * @param string $url
     * @return mixed
     */
    private function getDataFromUrl(string $url)
    {
        $response = (new Client)->request('GET', $url);

        return json_decode($response->getBody(), true);
    }
}