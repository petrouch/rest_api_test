<?php

namespace App\Http\Controllers;


use App\Country;
use App\PhoneBook;
use App\Rules\PhoneNumberRule;
use App\Timezone;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Rules\HasCountry;
use App\Rules\HasTimezone;
use Illuminate\Validation\Rule;
use Propaganistas\LaravelPhone\PhoneNumber;

/**
 * Class PhoneBookController
 * @package App\Http\Controllers
 */
class PhoneBookController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecords()
    {
        return response()->json(PhoneBook::with(['country', 'timezone'])->get());
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecordById(int $id)
    {
        return response()->json(PhoneBook::findOrFail($id)->with(['country', 'timezone'])->first());
    }

    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecordsByName(string $name)
    {
        $name = urldecode($name);

        return response()->json(PhoneBook::whereRaw("concat(first_name, ' ', last_name) like '%{$name}%' ")
            ->with(['country', 'timezone'])->get());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        };

        $record = PhoneBook::create($this->changeRequest($request));

        return response()->json($record, 201);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        };

        $record = PhoneBook::findOrFail($id);

        $record->update($this->changeRequest($request));

        return response()->json($record, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function destroy(int $id)
    {
        PhoneBook::findOrFail($id)->delete();

        return response('Deleted Successfully', 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validateData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255', new PhoneNumberRule($request->country_code)],
            'country_code' => [
                'required', 'string', 'max:255', new HasCountry
            ],
            'timezone' => [
                'required', 'string', 'max:255', new HasTimezone
            ]
        ]);

        if (!$validator->fails()) {
            $validator = Validator::make($request->all(), [
                'phone' => ['phone:' . $request->country_code],
            ]);
        }

        return $validator;
    }

    /**
     * Get country_id and timezone_id from Database and add to Request $request object
     * @param Request $request
     * @return array
     */
    private function changeRequest(Request $request) {
        $request->request->add([
            'country_id'  => Country::where('code', strtoupper($request->country_code))->first()->id,
            'timezone_id' => Timezone::where('name', stripslashes($request->timezone))->first()->id,
            'phone'       => PhoneNumber::make($request->phone, $request->country_code)->formatE164()
        ]);

        return $request->all();
    }
}