<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\RefreshApiInterface;
use App\Timezone;
use GuzzleHttp\Client;

/**
 * Class TimezoneController
 * @package App\Http\Controllers
 */
class TimezoneController extends Controller implements RefreshApiInterface
{
    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function refreshData()
    {
        $new_timezones = $this->getDataFromUrl('https://experum.ru/te/timezones');

        $old_timezones = Timezone::all()->keyBy('name');

        foreach ($new_timezones as $name => $timezone) {
            $name = stripcslashes($name);

            if (isset($old_timezones[$name]))
            {
                if ($old_timezones[$name]->offset !== $timezone['offset']) {
                    $old_timezones[$name]->offset = $timezone['offset'];
                }
            } else {
                $old_timezones[$name] = new Timezone();

                $old_timezones[$name]->offset = $timezone['offset'];
                $old_timezones[$name]->name = $name;
            }

            $old_timezones[$name]->save();
        }

        return response('Refreshed Successfully', 200);
    }

    /**
     * @param string $url
     * @return mixed
     */
    private function getDataFromUrl(string $url)
    {
        $response = (new Client)->request('GET', $url);

        return json_decode($response->getBody(), true);
    }
}