<?php

namespace App\Rules;

use App\Country;
use Illuminate\Contracts\Validation\Rule;


/*
 * Has country checking request value in Country table
 */
class HasCountry implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $country = Country::where('code', strtoupper($value))->first();

        return ($country);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute should be in Country tables';
    }
}