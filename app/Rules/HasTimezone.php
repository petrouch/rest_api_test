<?php

namespace App\Rules;

use App\Timezone;
use Illuminate\Contracts\Validation\Rule;


/*
 * Has timezone checking request value in Timezone table
 */
class HasTimezone implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $timezone = Timezone::where('name', stripslashes ($value))->first();

        return ($timezone);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute should be in Timezone tables';
    }
}