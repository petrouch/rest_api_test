<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class PhoneBook extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone', 'country_id', 'timezone_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'phone_books';

    /**
     * Get the country record associated with the record.
     */
    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }

    /**
     * Get the timezone record associated with the record.
     */
    public function timezone()
    {
        return $this->hasOne('App\Timezone', 'id', 'timezone_id');
    }
}